# 2048 in TI-basic for TI-86
Split up into multiple programs because the TI-86 doesn't have functions.

## Source
.src files are UTF-8 source code. Need to be manually typed into the calculator
or emulator, unless you can find a compiler capable of handling them.

Be wary of typos.

Ignore lines starting with //, they are comments, and as far as I know the PRGM
editor in the TI-86 does not support comments.

I have used the UTF-8 character "→" for the store operation, since the
character on the calculator looks similar. Other computer-editable dialects use
the two-character string "->" instead.

Similarly, I have used "≠" for not-equal, and I don't remember what other
dialects use.

Also, a few Greek characters are used to shorten variable names (notably τ for
temporary and Σ for the score (or sum)) and thus decrease program size, because
why the hell not? I have access to unicode, and the calculator supports those
specific characters.

## Binaries
ti2048.86g.b64 is a base64-encoded group package of the "binaries" extracted
from an emulator after manually typing the source in as per the above
instructions, grouped rather than individual files for easier transfer to a
calculator/emulator.

Base64-encoded because the 86p/g format is effectively a binary format and
rather sensitive to even small changes that may be difficult to detect.

## ti2048
Entry point of the game, calls the other programs when needed.
Use arrow keys to push numbers, EXIT to quit, and CLEAR to reset the board for
a new game.

## ti2AN
Adds a new number to the board.

## ti2MR
Mirror the board left/right.

## ti2NV
Inverts X/Y axis for the board.

## ti2LT
Main "push/combine" program, pushes numbers to the left, and combines equal
numbers.

# ti2RT
Pushes the numbers to the right, by first mirroring the board, calling
ti2048LT, then mirroring again.

## ti2UP
Pushes numbers up by inverting the axes, pushing left, then inverting again.

## ti2DN
Pushes numbers down by flipping the board upside-down through the built-in
function rSwap, inverting the board, pushing left, then reversing the invert
and flip.

# bas2048
Implementations of the game 2048 in various dialects of Basic.

## QBasic
QBasic seems to work well enough. Tested in both QB64 on Linux, and QBasic 1.1
under MS-DOS 6.22 in QEmu.

There seems to be an issue in QEmu that the arrow keys register twice.
Unfortunately I don't have an actual DOS machine (that works) to test on, so
I don't know if it's an issue with QEmu or something else.

## TI-86
Slow, but works. My TI-86 calculator hasn't worked in many years, but the
application runs well (depending on your definition of "well") under an
emulator.
